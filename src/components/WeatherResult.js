import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View,Text,Image } from 'react-native';

export function WeatherResult(props) {
    return (
        <View style={styles.background}>
            <View style={styles.primary_background}>
                <Image src={props.weatherForecast.weather_icon}></Image>
                <Text>{props.weatherForecast.weather_description}</Text>
                <Text>{props.weatherForecast.temperature}</Text>
            </View>
            <View style={styles.secondary_background}>
                <Text>{props.weatherForecast.wind_speed}</Text>
                <Text>{props.weatherForecast.humidity}</Text>
            </View>
        </View>

    );
}

const mapStateToProps = (state) => {
    return {
        weatherForecast: state.forecastReducer.weatherForecast,
    }
}

export default connect(mapStateToProps)(WeatherResult)

const styles = StyleSheet.create({
    background: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    primary_background: {
        backgroundColor: '#FFFFFF',
        opacity: 0.80,
        borderRadius: 12,
        height: 270,
        width: 283,

    },
    secondary_background: {
        backgroundColor: '#404491',
        borderRadius: 12,
        opacity: 1,
        height: 70,
        width: 230,
        marginTop: -35
    }
});
