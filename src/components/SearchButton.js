import React from 'react';
import { View, Image, TouchableOpacity,StyleSheet } from 'react-native';

export function SearchButton(props) {
    return (
        <View>
            <TouchableOpacity style={styles.search_button} activeOpacity={0.9} onPress={props.onPress}>
            <Image source={{ uri: 'https://image.flaticon.com/icons/png/512/125/125502.png' }} style={styles.icon}>
            </Image> 
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    search_button: {
        borderRadius: 12,
        marginTop: 40,
        width: 75,
        height: 60,
        backgroundColor:'#FFFFFF',
        marginLeft: 10,
        opacity: 0.7
    },
    icon:{
        width:40,
        height:40,
        margin : 9,
        marginLeft: 20
    }
});