import HeaderForecast from './HeaderForecast';
import WeatherResult from './WeatherResult';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { chosenCity } from "../actions/forecast";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ChooseCityInput } from './ChooseCityInput';





export function Forecast(props) {
    return (
        <View style={styles.input}>
            <HeaderForecast />
            <WeatherResult />
            <ChooseCityInput onChange={(event) => props.chosenCity(event.target.value)} value={props.city} />
        </View>
    );
}

const mapStateToProps = (state) => {
    return {
        city: state.forecastReducer.city
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        chosenCity
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Forecast)

const styles = StyleSheet.create({
    input: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#70A8A8'
    }
});

