import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';

export function HeaderForecast(props) {
    return (
        <View style={styles.container_header}>
            <Text style={styles.city_name}>{props.weatherForecast.city}</Text>
            <Text style={styles.date}>{props.weatherForecast.localtime}</Text>
        </View>
    );
}

const mapStateToProps = (state) => {
    return {
        weatherForecast:state.forecastReducer.weatherForecast,
    }
}


export default connect(mapStateToProps)(HeaderForecast)

const styles = StyleSheet.create({
    container_header: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    city_name: {
        color: 'white',
        fontWeight:'bold',
        fontSize:75
    },
    date:{
        color: 'white',
        fontWeight:'bold',
        fontSize:20,
    }
});
